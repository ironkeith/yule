require 'yule/formatter'

module Yule
  module Builder
    class Tree

      # Define the structure of an ascii tree where each row
      # is two units large than the previous, and all units are centered.
      # Returns a list of arrays, each representing one row of the
      # ascii art. +true+ value should be filled, +false+ should be spaces
      #
      #     *  
      #    *** 
      #   *****
      #
      # ==== Example
      #
      #   Yule::Builder::Tree.build 1
      #   => [[true]]
      #   Yule::Builder::Tree.build 2
      #   => [[false, true, false], [true, true, true]]
      def build(rows)
        (1..rows).map do |n|
          padding = Array.new rows - n, false
          items =  Array.new (2 * n) - 1, true
          padding.concat items.concat(padding)
        end
      end

    end
  end
end