require 'yule/utils/mirror'

module Yule
  module Builder
    class Greeting

      # Define the structure of an ascii art 'X' that gets 
      # increasingly large based on the requested number of +rows+. 
      # Returns a list of arrays, each representing one row of the
      # ascii art. +true+ value should be filled, +false+ should be spaces
      #
      #   Yule::Builder::Greeting.build 1 
      #   => [[true]]
      #   Yule::Builder::Greeting.build 2 
      #   => [[true, false, true], [false, true, false], [true, false, true]]
      def build(rows)
        return [[true]] if rows == 1
        # start off building the top half of the 'X' (which is a 'V')
        top = (1..rows).map do |n|
          # start by building the left half...
          row = Array.new(rows, false)
          row[n - 1] = true
          # ...then fill in the right half as a mirror image
          Yule::Utils.mirror row
        end
        # turn our 'V' into an 'X' by mirroring the rows
        Yule::Utils.mirror top
      end

    end
  end
end