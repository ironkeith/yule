module Yule
  module Utils

    # Takes an array and returns a mirrored version
    #
    #   mirror.call [1, 2, 3] # => [1, 2, 3, 2, 1]
    def Utils.mirror(arr)
      duped = arr.map { |i| i.dup }
      arr.concat duped.reverse!.drop(1)
    end

  end
end