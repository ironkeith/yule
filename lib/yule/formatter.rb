module Yule
  class Formatter
    # Takes a list of arrays, and converts them to a string.
    # Each array will be its own line in the string. +true+
    # values are converted to '*', +false+ to ' '.
    #
    #   Yule::Formatter.format [[true, false]] # => "* \n"
    def self.format(structure)
      output = []
      structure.each do |row|
        output << row.map { |i| i ? '*' : ' ' }.join
      end
      output.join("\n") + "\n"
    end
  end
end