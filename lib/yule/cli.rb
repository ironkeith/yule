require 'thor'
require 'yule'
require 'zlib'
require 'base64'

module Yule

  class CLI < Thor
    include Thor::Actions

    desc "tree [ROWS]", "Builds a pretty tree with the requested number of rows."
    def tree(rows)
      rows = rows.to_i
      if rows < 1 then return say "It was an ugly tree anyway.", :red end
      if rows > 40
        say "Where do you think you're gonna put a tree that big?", :yellow
        say "  1. Bend over and I'll show you."
        say "  2. It's not going in the yard, Russ. It's going in the living room."
        confirm = ask "Select:", limited_to: ["1", "2"]
        if confirm == "1" then return say Zlib.inflate(Base64.decode64(Yule::TOO_MANY_ROWS)) end
      end
      say output(:tree, rows), :green
    end

    desc "greeting [ZEAL]", "Get yourself some yuletide cheer."
    def greeting(rows)
      rows = rows.to_i
      if rows < 1 then return say "happy holidays.", :red end
      say "Merry", :yellow
      x = if rows > 1 then output(:greeting, rows) else 'X' end
      say x, :green
      say "Mas", :yellow
    end

    private

    def output(type, rows)
      builder = if type == :tree then Yule::Builder::Tree.new else Yule::Builder::Greeting.new end
      structure = builder.build rows
      Yule::Formatter.format structure
    end
  end
end