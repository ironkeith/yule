require "spec_helper"
require 'yule'

RSpec.shared_examples "a yule builder" do
  it { is_expected.to respond_to(:build).with(1).argument }
end

RSpec.describe Yule do
  it "has a version number" do
    expect(Yule::VERSION).not_to be nil
  end 
end

RSpec.describe Yule::Builder::Tree do
  it_behaves_like "a yule builder"

  it "builds a tree with 1 row" do
    expect(Yule::Builder::Tree.new.build(1)).to eq([[true]])
  end

  it "builds a tree with 2 rows" do
    output = [
      [false, true, false],
      [true, true, true]
    ]
    expect(Yule::Builder::Tree.new.build(2)).to eq(output)
  end
end

RSpec.describe Yule::Builder::Greeting do
  it_behaves_like "a yule builder"

  it "builds a greeting with 1 row" do
    expect(Yule::Builder::Greeting.new.build(1)).to eq([[true]])
  end

  it "builds a greeting with 2 rows" do
    output = [
      [true, false, true],
      [false, true, false],
      [true, false, true]
    ]
    expect(Yule::Builder::Greeting.new.build(2)).to eq(output)
  end
end

RSpec.describe Yule::Formatter do
  it "converts lists of arrays to strings" do
    input = [[true, false], [false, true]]
    output = "* \n *\n"
    expect(Yule::Formatter.format(input)).to eq(output)
  end
end