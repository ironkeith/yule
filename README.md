# Yule

Merry Christmas! Get into the spirit in the best way possible — via the command line.

## Installation

(If I had published this gem, you could) add this line to your application's Gemfile:

```ruby
gem 'yule'
```

And then execute:

    $ bundle

Or install it yourself as:

    $ gem install yule

## Usage

Once installed, use the `yule` command to get psyched for the season. (Make sure you install it first with `bundle exec rake install` because this hasn't been published yet.)

    $ yule tree [ROWS]     # => outputs a Christmas tree
    $ yule greeting [ZEAL] # => outputs a seasonal greeting
    $ yule tree 3
      *  
     *** 
    *****
    $ yule greeting 3
    Merry
    *   *
     * * 
      *  
     * * 
    *   *
    Mas

## Development

After checking out the repo, run `rake spec` to run the tests. You can also run `bin/console` for an interactive prompt that will allow you to experiment.

To install this gem onto your local machine, run `bundle exec rake install`. To release a new version, update the version number in `version.rb`, and then run `bundle exec rake release`, which will create a git tag for the version, push git commits and tags, and push the `.gem` file to [rubygems.org](https://rubygems.org).

## License

The gem is available as open source under the terms of the [MIT License](http://opensource.org/licenses/MIT).
